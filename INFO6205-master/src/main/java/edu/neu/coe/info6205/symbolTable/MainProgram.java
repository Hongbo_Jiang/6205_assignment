/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.coe.info6205.symbolTable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author 15142087777的AW
 */
public class MainProgram {
    /**
     * 根据min和max随机生成一个范围在[min,max]的随机数，包括min和max
     * @param min
     * @param max
     * @return int
     */
    public static int getRandom(int min, int max){
        Random random = new Random();
        return random.nextInt( max - min + 1 ) + min;
    }
    
    /**
     * 根据min和max随机生成count个不重复的随机数组
     * @param min
     * @param max
     * @param count
     * @return int[]
     */
    public static int[] getRandoms(int min, int max, int count){
        int[] randoms = new int[count];
        List<Integer> listRandom = new ArrayList<Integer>();

        if( count > ( max - min + 1 )){
            return null;
        }
        // 将所有的可能出现的数字放进候选list
        for(int i = min; i <= max; i++){
            listRandom.add(i);
        }
        // 从候选list中取出放入数组，已经被选中的就从这个list中移除
        for(int i = 0; i < count; i++){
            int index = getRandom(0, listRandom.size()-1);
            randoms[i] = listRandom.get(index);
            listRandom.remove(index);
        }

        return randoms;
    }
    
    public static void main(String[] args)
    {
        double totalOrignalDepth = 0;
        double totalEditedDepth = 0;
        int N = 130;/*numbers of keys*/
        int M = 200;/*the range of key values*/
        
        for(int count = 0; count<1000; count++)
        {
            /*generate N keys value range from 0 to M*/
            BSTSimple bSTSimple = new BSTSimple();
            
            int[] randoms = new int[N];
            randoms = getRandoms(1,N,N);
            /*System.out.println(Arrays.toString(randoms));*/
        
            for(int i = 0; i<N; i++)
            {
                bSTSimple.put(randoms[i], getRandom(1,M));
            }
        
            totalOrignalDepth += bSTSimple.findDeep(bSTSimple.getRoot());
            
            for(int changeCount = 0; changeCount<100000; changeCount++)
            {
                int random = getRandom(1,2);
                
                if(random == 1)/*DO INSERTION*/
                {
                    bSTSimple.put(getRandom(1,N), getRandom(1,M));
                }
                else if(random == 2)/*DO DELETION*/
                {
                    bSTSimple.delete(getRandom(1,N));
                }
            }
            
            totalEditedDepth += bSTSimple.findDeep(bSTSimple.getRoot());
        }
        
        double averageOrignalDepth = totalOrignalDepth/1000;
        double averageEditedDepth = totalEditedDepth/1000;
        
        System.out.println("N = "+ N);
        System.out.println("\n");
       
        System.out.println(totalOrignalDepth);
        System.out.println(averageOrignalDepth);
        /*System.out.println("lgN = "+ Math.log((double)N)/Math.log((double)2));
        System.out.println(averageOrignalDepth/(Math.log((double)N)/Math.log((double)2)));*/
        System.out.println("lgN = "+ Math.log((double)N));
        System.out.println(averageOrignalDepth/(Math.log((double)N)));
        System.out.println("\n");
        
        System.out.println(totalEditedDepth);
        System.out.println(averageEditedDepth);
        System.out.println("N^1/2 = "+Math.sqrt(N));
        System.out.println(averageEditedDepth/(Math.sqrt(N)));
        System.out.println("\n");
    }
    
}
